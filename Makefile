TARGET = rtf2xhtml
CONFIG = debug
DEBUG = debug
RELEASE = release
SRC = src
PREFIX = /usr/local

all:
	cd $(SRC) && $(MAKE) $(CONFIG)
	if [ $$? ]; then cp $(CONFIG)/$(TARGET) .; fi

both:
	cd $(SRC) && $(MAKE) $(DEBUG)
	cd $(SRC) && $(MAKE) $(RELEASE)

install:
	$(MAKE) $(RELEASE)
	if [ $$? ]; then cp $(CONFIG)/$(TARGET) $(PREFIX)/bin; fi

clean:
	rm -rf debug
	rm -rf release
	rm -f rtf2xhtml
