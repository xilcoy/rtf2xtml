/***************************************************************************
              options.c  -  This file is for getting global options
                            and reading options from an ini file
                             -------------------
    begin                : Mon Mar 31 2004
    copyright            : (C) 2004 by Coy D. Barnes
    email                : coy.barnes@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include <options.h>
#include <rtfparser.h>
#include <log.h>

static char token[256];
static char line[512];
static FILE* in = NULL;
static FILE* out = NULL;

/* Gets the next token from the file */
static int get_token()
{
   int c,i;
   
   token[0]=0;
   i=0;

   if (in==NULL)
      return -1;

   do
   {
      c = fgetc(in);
      if (c==EOF)
         return -1;
   } while (c == ' ' || c == '\t' || c == '\r' || c == '\n');
   
   for(;;c=fgetc(in))
   {
      /* Treat anything between quotes as a token */
      if (c=='"')
      {
         for(;;)
         {
            c = fgetc(in);
            if (c=='"')
               break;
            if (c==EOF)
               return -1;
            token[i++]=c;
         }
         break;
      }
      else if (c=='\'')
      {
         for(;;)
         {
            c = fgetc(in);
            if (c=='\'')
               break;
            if (c==EOF)
               return -1;
            token[i++]=c;
         }
         break;
      }
      /* ; Marks the beggining of a comment */
      else if (c==';')
      {
         printl(DEBUG,"get_token","Comment found\n");
         if (token[0]!=0)
         {
            fseek(in,-1,SEEK_CUR);
            break;
         }
         fgets(line,511,in);
         return get_token();
         break;
      }
      /* =, [, and ] are special characters treated as tokens themselves */
      else if (c=='=' || c=='[' || c==']')
      {
         if (token[0]!=0)
         {
            fseek(in,-1,SEEK_CUR);
            break;
         }
         else
            token[i++]=c;
         break;
      }
      /* spaces newlines and tabs are delimeters */
      else if (c==' ' || c=='\r' || c=='\n' || c=='\t')
         break;
      else
         token[i++]=c;
   }
   token[i]=0;
   printl(DEBUG,"get_token","Found \"%s\"\n",token);
   return 0;
}

/* Macro that checks for on/off, 1/0, or ignore */
#define options_set(OP) \
   if(strcmp(a,"on")==0 || strcmp(a,"1")==0) \
      OP=1; \
   else if(strcmp(a,"off")==0 || strcmp(a,"0")==0 || strcmp(a,"ignore")==0) \
      OP=0; \
   else \
      printl(WARNING,"options_parse","unknown parameter %s for option %s\n",a,o);

/* Validate and set options */
static void options_parse(const char *s, const char *o, const char *a)
{
   if(strcmp(s,"program")==0)
   {
      if(strcmp(o,"font")==0)
      {
         options_set(options.font);
      }
      else if(strcmp(o,"fontsize")==0)
      {
         options_set(options.fontsize);
      }
      else if(strcmp(o,"expandtabs")==0)
      {
         options_set(options.expandtabs);
      }
      else if(strcmp(o,"color")==0)
      {
         options_set(options.color);
      }
      else if(strcmp(o,"align")==0)
      {
         options_set(options.align);
      }
      else if(strcmp(o,"justify")==0)
      {
         options_set(options.justify);
      }
      else if(strcmp(o,"indent")==0)
      {
         set_indent(a);
      }
      else
      {
         printl(WARNING,"options_parse","unknown option %s\n",o);
      }
   }
   else
   {
      printl(WARNING,"options_parse","unknown section %s\n",s);
   }
}

/* Initialize options to default */
void options_init(void)
{
   options.font=1;
   options.fontsize=1;
   options.color=1;
   options.align=1;
   options.justify=1;
   options.stylesheet=0;
   options.expandtabs=0;
}

/* Read the ini file to obtain settings */
void options_read(const char* filename)
{
   char section[64];
   char option[64];
   char attrib[128];
   
   in=fopen(filename,"r");

   if (in==NULL)
   {
      printl(WARNING,"options_read","could not open %s\n",filename);
      return;
   }
 
   section[0]=0;
   option[0]=0;
   attrib[0]=0;
   for(;;)
   {
      if(get_token() == -1)
      {
         break;
      }
      /* [Section] */
      if(strcmp(token,"[")==0)
      {
         if(get_token() == -1)
         {
            printl(ERROR,"options_read","section contains no data\n");
            break;
         }
         strncpy(section,token,63);
         if(get_token() == -1)
         {
            printl(ERROR,"options_read","section not properly ended\n");
            break;
         }
         if(strcmp(token,"]")!=0)
         {
            printl(ERROR,"options_read","invalid section name\n");
            break;
         }
      }
      /* illegal = */
      else if (strcmp(token,"=")==0)
      {
         printl(ERROR,"options_read","floating =\n");
         break;
      }
      /* option=arg */
      else
      {
         strncpy(option,token,63);
         printl(DEBUG,"options_read","Option %s found\n",option);
         get_token();
         if(strcmp(token,"=")!=0)
         {
            printl(ERROR,"options_read","I was expecting = but found \"%s\"\n",token);
            break;
         }
         get_token();
         strncpy(attrib,token,127);
         options_parse(section,option,attrib);
      }
   }
   fclose(in);
}

/* Write current options to configuration file */
void options_write(const char* filename)
{
   out=fopen(filename,"w");

   fprintf(out,";File automatically generated by options_write function of rtf2xhtml\n");
   fprintf(out,"[program]\n");
   fprintf(out,"font=%d\n",options.font);
   fprintf(out,"fontsize=%d\n",options.fontsize);
   fprintf(out,"color=%d\n",options.color);
   fprintf(out,"align=%d\n",options.align);
   fprintf(out,"justify=%d\n",options.justify);
   fprintf(out,"expandtabs=%d\n",options.expandtabs);
   fprintf(out,"indent=\"%s\"\n",SPACER);
}
