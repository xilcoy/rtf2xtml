/***************************************************************************
                          main.c  -  This file parses the command line
                                     and calls the rtf2xhtml function
                             -------------------
    begin                : Mon Mar 8 2004
    copyright            : (C) 2004 by Coy D. Barnes
    email                : coy.barnes@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include <rtfparser.h>
#include <log.h>
#include <options.h>

#define MAXFILES 100

static void usage(const int);
static void help();
static int parse_command_line(const int, const char**);

static int usecfg;
static int save;
char filelist[MAXFILES][256];
char cfgfile[256];
int files;

static void usage(int error)
{
   FILE *fp;
   if (error)
      fp = stderr;
   else
      fp = stdout;
   fprintf(fp,"usage: rtf2xhtml [options] [files]\n"
              "      -h, --help                 Display help\n"
              "      -i, --indent=SPACER        Change the indent string to SPACER\n"
              "      -t, --expandtabs           Expand tabs to 8 &nbsp;\n"
              "      -s, --setings=FILE         Use FILE for settings\n"
              "      -w, --save-settings        Save the current settings to a file\n"
              "      -v, --verbose              Print debug messages\n");
   fprintf(fp,"      --very-verbose             Print lots of extra messages\n" 
              "      --ignore-color             Do not use colors in html\n"
              "      --ignore-font              Do not change the font\n"
              "      --ignore-fontsize          Do not set the font size\n"
              "      --ignore-justify           Do not set alignment to justify\n"
              "      --ignore-align             Do not set alignment at all\n"
              "");
}

static void help()
{
   printf("NAME:        rtf2xhtml\n"
          "DESCRIPTION: converts a Rich Text File (rtf) into a properly format"
          "ted xhtml\n"
          "             document that should be very easy to edit.\n"
          "USAGE:       rtf2xhtml [options] [files]\n"
          "(C) 2004 by Coy Don Barnes\n"
          "This program is free software; you can redistribute it and/or modif"
          "y it under\n"
          "the terms of the GNU General Public License as published by the Fre"
          "e\n"
          "Software Foundation; either version 2 of the License, or (at your o"
          "ption) any\n"
          "later version.\n"
          "");
}

static int parse_command_line(const int argc, const char **argv)
{
   int i;
   files = 0;
   for(i = 0; i < argc; i++)
   {
      if (argv[i][0] == '-')
      {
         switch (argv[i][1])
         {
            case 'h':
               help();
               break;
            case 'i':
               if (strlen(argv[i]) > 2)
               {
                  set_indent(&argv[i][2]);
               }
               else if (argc > i+1)
               {
                  set_indent(argv[i+1]);
                  i++;
               }
               else
               {
                  fprintf(stderr,"-i Requires an argument\n");
                  return 2;
               }
               break;
            case 's':
               if (strlen(argv[i]) > 2)
               {
                  usecfg = 1;
                  strcpy(cfgfile,&argv[i][2]);
               }
               else if (argc > i+1)
               {
                  usecfg = 1;
                  strcpy(cfgfile,argv[i+1]);
                  i++;
               }
               else
               {
                  fprintf(stderr,"-s Requires an argument\n");
                  return 2;
               }
               break;
            case 't':
               options.expandtabs=1;
               break;
            case 'v':
               log_level=DEBUG;
               break;
            case 'w':
               save=1;
               break;
            case '-':
               if (strcmp(&argv[i][2],"help")==0)
               {
                  help();
                  return 1;
               }
               else if (strncmp(&argv[i][2],"indent=",7)==0)
               {
                  set_indent(&argv[i][9]);
               }
               else if (strcmp(&argv[i][2],"expandtabs")==0)
               {
                  options.expandtabs=1;
               }
               else if (strncmp(&argv[i][2],"settings=",9)==0)
               {
                  usecfg=1;
                  strcpy(cfgfile,&argv[i][11]);
               }
               else if (strcmp(&argv[i][2],"save-settings")==0)
               {
                  save=1;
               }
               else if (strcmp(&argv[i][2],"verbose")==0)
               {
                  log_level=DEBUG;
               }
               else if (strcmp(&argv[i][2],"very-verbose")==0)
               {
                  log_level=ALL;
               }
               else if (strncmp(&argv[i][2],"ignore-",7)==0)
               {
                  if (strcmp(&argv[i][9],"font")==0)
                  {
                     options.font=0;  
                  }
                  else if (strcmp(&argv[i][9],"fontsize")==0)
                  {
                     options.fontsize=0;
                  }
                  else if (strcmp(&argv[i][9],"align")==0)
                  {
                     options.align=0;
                  }
                  else if (strcmp(&argv[i][9],"justify")==0)
                  {
                     options.justify=0;
                  }
                  else if (strcmp(&argv[i][9],"color")==0)
                  {
                     options.color=0;
                  }
               }
               else 
               {
                 fprintf(stderr,"Unknown option %s\n", argv[i]);
                 usage(1);
                 return 2;
               }
               break;
            default:
               fprintf(stderr,"Unknown option %s\n", argv[i]);
               usage(1);
               return 2;
         }
      }
      else 
      {
         if (files < MAXFILES)
         {
            strncpy(filelist[files],argv[i],254);
            files++;
         }
      }
   }
   return 0;
}

int main(const int argc, const char **argv)
{
   int i=0;
   int test;
   char inputfile[256],outputfile[256];
   char c;
   char *ptr;

   usecfg=0;
   save=0;

   strcpy(cfgfile,"settings.ini");
   
   log_level = INFO;
   log_file = 1;
   
   options_init();
   options_read(cfgfile);

   test = parse_command_line(argc - 1, &argv[1]);
   if (test == 1)
      return 0;
   else if (test != 0)
      return test;

   if(save)
      options_write(cfgfile);
   else if(usecfg)
      options_read(cfgfile);
   
   if (files==0)
   {
      printf("No input files given, enter interactive mode? ");
      c=getc(stdin);
      if (c!='Y' && c!='y')
      {
         return 0;
      }
      else {
         printl(INFO,"main","Entering interactive mode\n");
         printf("type / to quit\n");
      }
   }
   
   for(;;)
   {
      if (files==0)
      {
         printf("Enter a filename: ");
         scanf("%s",inputfile);
         if (strcmp(inputfile,"/")==0)
         {
               return 0;
         }
      }
      else if (i < files)
         strncpy(inputfile,filelist[i],254);
      else break;

      strcpy(outputfile,inputfile);
      ptr = strstr(outputfile,".rtf");
      if (ptr != NULL)
      {
         *ptr = 0;
         ptr = NULL;
      }
      strcat(outputfile,".html");
      rtf2xhtml(inputfile,outputfile);
      i++;
   }
   return 0;
}
