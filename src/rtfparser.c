/***************************************************************************
              rtfparser.c  -  This file contains most of the necessary code
                              to convert a document from rtf to html
                             -------------------
    begin                : Mon Mar 8 2004
    copyright            : (C) 2004 by Coy D. Barnes
    email                : coy.barnes@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <rtfparser.h>
#include <stack.h>
#include <log.h>
#include <options.h>

/*Types of decoration */
#define PLAIN 0               /* 00000000 00000000 */
#define BOLD 1                /* 00000000 00000001 */
#define UNDERLINE 2           /* 00000000 00000010 */
#define ITALIC 4              /* 00000000 00000100 */
#define CAPS 8                /* 00000000 00001000 */
#define SHADOW 16             /* 00000000 00010000 */
#define STRIKE 32             /* 00000000 00100000 */
#define STRIKE2 64            /* 00000000 01000000 */
#define SUPER 128             /* 00000000 10000000 */
#define SUB 256               /* 00000001 00000000 */
#define OUTLINE 512           /* 00000010 00000000 */
#define SCAPS 1024            /* 00000100 00000000 */

/* Font family */
#define NIL 0
#define ROMAN 1
#define SWISS 2
#define MODERN 3
#define SCRIPT 4
#define DECOR 5
#define TECH 6
#define BIDI 7

/* Alignment */
#define LEFT 0
#define RIGHT 1
#define CENTER 2
#define JUSTIFY 3
#define ALIGNCLEAR 65529
#define PWRITE   4   /* Determine if the paragraph is being written */

#define CHARACTER 0
#define INDENT 9
#define NEWLINE 10
#define SPACE 20

#define AUTO 0

/* Variables */
static state default_state = 
{
   0,       /* font */
   24,      /* fontsize */
   NIL,     /* fontfamily */
   PLAIN,   /* decoration */
   0,       /* style */
   0,       /* character_set */
   0,       /* language */
   0,       /* bg */
   0,       /* fg */
   LEFT,    /* Alignment */
};
char SPACER[11]="   \0\0\0\0\0\0\0";
static char font_table[MAXFONTS][MAXFONTNAME+1];
static char font_family[MAXFONTS];
static int color_table[MAXCOLORS];
static state stylesheet[MAXSTYLES];
static int depth;
static state current_state;
static state previous_state;
static stack* ss;
static stack* hts;
static FILE *in, *out;
static char indent[DEPTH+1];
static int xpos=0;
static char last_char;

/* Functions */
static void push_tag(const char*, const char*,int);
static char* peek_tag();
static int  pop_tag();
static void change_state();
static void push_state();
static int  pop_state();
static void set_defaults(const char*);
static char* translate();
static void plain_text();
static void paragraph_default();
static void skip();
static void read_font_table();
static void read_header();
static void read_color_table();
static void read_style_sheet();
static void initialize();
static void cleanup();
static void putstring(const char *);

/* 
Whenever a tag with /deffoo comes up the
default state needs to be set
*/
static void set_defaults(const char* str)
{
   if (strncmp(str,"lang",4)==0)
   {
      default_state.language=atoi(&str[4]);
   }
   else if (strncmp(str,"format",6)==0) 
   {
      /* TODO: set format  */
      ;
   }
   else if (str[0]=='f') 
   {
      default_state.font=atoi(&str[1]);
   }
}

static char* peek_tag()
{
   static char itag[TAGSIZE+1];

   memset(itag,0,TAGSIZE+1);
   stack_peek(hts,itag);
   return itag;
}

static void push_tag(const char* tag, const char* att, int single)
{
   char otag[TAGSIZE+1];

   memset(otag,0,TAGSIZE+1);
   strncpy(otag,tag,TAGSIZE);
   if (!single)
   {
      stack_push(hts,otag);
   }
   if (last_char!=NEWLINE && last_char!=INDENT)
   {
      fprintf(out,"\n");
   }
   if (last_char!=INDENT )
   {
      fprintf(out,"%s",indent);
   }
   if (!single)
   {
      fprintf(out,"<%s%s>\n",otag,att);
   }
   else
   {
      fprintf(out,"<%s%s />\n",otag,att);
   }
   if (strlen(indent) < DEPTH && !single) 
   {
      strcat(indent,SPACER);
   }
   xpos=fprintf(out,"%s",indent);
   last_char=INDENT;
}

static int pop_tag(char* tag)
{   
   int size;
   char itag[TAGSIZE+1];

   memset(itag,0,TAGSIZE+1);
   if(stack_pop(hts,itag) == 0)
   {
      return 0;
   }
   strncpy(tag,itag,TAGSIZE);
   if ((size=strlen(indent)) > 0)
   {
      indent[size-strlen(SPACER)]=0;
   }
   if (last_char!=NEWLINE && last_char!=INDENT)
   {
      fprintf(out,"\n");
   }
   if (last_char!=INDENT )
   {
      fprintf(out,"%s",indent);
   }
   else 
   {
      xpos+=fseek(out,-((signed)strlen(SPACER)),SEEK_CUR);
   }
   fprintf(out,"</%s>\n",itag);
   xpos=fprintf(out,"%s",indent);
   last_char=INDENT;
   return 1;
}

static void change_state()
{
   char attrib[1024];
   char tempstr[256];
   char* ptr;
   int tmpNum,force=0;

   attrib[0]=0;
   tempstr[0]=0;
   if (previous_state.paragraph != current_state.paragraph) 
   {
      if (options.align)
      {
         switch(current_state.paragraph & 0x3)
         {
         case LEFT:
            strcat(attrib," style=\"text-align: left;\" ");
            break;
         case RIGHT:
            strcat(attrib," style=\"text-align: right;\"");
            break;
         case CENTER:
            strcat(attrib," style=\"text-align: center;\"");
            break;
         case JUSTIFY:
            if (options.justify)
            {
               strcat(attrib," style=\"text-align: justify;\"");
            }
            break;
         }
      }
      ptr=peek_tag();   
      if(strcmp(ptr,"span")==0) 
      {
         force=1;
         pop_tag(tempstr);
      }
      ptr=peek_tag();   
      if(strcmp(ptr,"p")==0)
      {
         pop_tag(tempstr);
      }
      push_tag("p",attrib,0);
      current_state.paragraph |= PWRITE;
      previous_state.paragraph=current_state.paragraph;
   }
   attrib[0]=0;
   tempstr[0]=0;
   if((memcmp(&previous_state,&current_state,sizeof(state)) != 0) || force)
   {
      strcpy(attrib," style=\"");
      /* background */
      if (options.color)
      {
         if (current_state.style != default_state.style) 
         {
            sprintf(tempstr,"background-color: #%06X; ",color_table[current_state.bg]);
            strcat(attrib,tempstr);
         }

         /* foreground */
         if (current_state.fg != default_state.fg) 
         {
            sprintf(tempstr,"color: #%06X; ",color_table[current_state.fg]);
            strcat(attrib,tempstr);
         }
      }
      /* font */
      if (options.font)
      {
         if (current_state.font != default_state.font) 
         {
            sprintf(tempstr,"font-family: '%s'; ",font_table[current_state.font]);
            strcat(attrib,tempstr);
         }
      }

      /* fontsize */
      if (options.fontsize)
      {
         if (current_state.fontsize != default_state.fontsize) 
         {
            sprintf(tempstr,"font-size: %dpt; ",current_state.fontsize / 2);
            strcat(attrib,tempstr);
         }
      }
      else if (options.headings)
      {
         /* TODO: Code for h1,h2, etc */
      }

      /* decoration  (BOLD, ITALIC, UNDERLINE, etc) */
      if (current_state.decoration != default_state.decoration) 
      {
         tmpNum = current_state.decoration;
         if (tmpNum & BOLD)
         {
            strcat(attrib,"font-weight: bold; ");
         }
         if (tmpNum & ITALIC)
         {
            strcat(attrib,"font-style: italic; ");
         }
         if ((tmpNum & STRIKE) && (tmpNum & UNDERLINE))
         {
            strcat(attrib,"font-decoration: line-through underline; ");
         }
         else if (tmpNum & STRIKE)
         {
            strcat(attrib,"font-decoration: line-through; ");
         }
         else if (tmpNum & UNDERLINE)
         {
            strcat(attrib,"font-decoration: underline; ");
         }
      }
      strcat(attrib,"\"");
      ptr=peek_tag();
      if(strcmp(ptr,"span")==0)
      {
         pop_tag(tempstr);
      }
      if (strcmp(attrib," style=\"\"")!=0) 
      {
         push_tag("span",attrib,0);
      }
   }

   previous_state=current_state;

}

static void push_state()
{
   depth++;
   stack_push(ss,&current_state);
}

static int pop_state()
{
   short write;
   state tmp;

   write = current_state.paragraph & PWRITE;
   if (depth>0)
   {
      depth--;
   }
   if(stack_pop(ss,&tmp)==0)
   {
      return 0;
   }
   current_state=tmp;
   if (write)
   {
      current_state.paragraph |= PWRITE;
   }
   else
   {
      current_state.paragraph &= ~PWRITE;
   }
   return 1;
}

/*FIXME: This should really parse out the code and its argument   *
 * right now it reads the code and argument in one big blob all   *
 * at once                                                        */
static char* translate()
{
   char c;
   static char code[40] = "";

   int i = 0;
   memset(code,0,40);
   for (i=0; (c = getc(in)); i++) 
   {
      if (i==40)
      {
         printl(ERROR,"translate","Maximum code length exceeded\n");
      }
      if (c==' ' || c==0x0d || c==0x0a) break;
      if (!(c>='0' && c<='9') && !(c>='A' && c<='Z') && !(c>='a' && c<='z') && c!='-' && c!='*') 
      {
         fseek(in,-1,SEEK_CUR);
         break;
      }
      if (c=='\'') 
      {
         code[i++]=c;
         c=getc(in);
         code[i++]=c;
         c=getc(in);
         code[i++]=c;
         code[i]=0;
         break;
      }
      if (i < 40) 
      {
         code[i]=c;
      }
   }

   /* \* is a comment (or at least this parser treats it as one */
   if (code[0]=='*') 
      skip();
  
   /* These tags are not written to the document  */
   /* FONTTABLE */
   else if (strcmp("fonttbl",code)==0) 
   {
      read_font_table();
   }
   /* COLORTABLE */
   else if (strcmp("colortbl",code)==0) 
   {
      read_color_table();
   }
   /* STYLESHEET */
   else if (strcmp("stylesheet",code)==0) 
   {
      read_style_sheet();
   }
   /* These are unimplemented as of yet */
   else if (strcmp("footer",code)==0 ||
          strcmp("header",code)==0 ||
          strcmp("footnote",code)==0 ||
          strcmp("pict",code)==0 ||
          strcmp("info",code)==0 )
   {
      /* Treat section as comment */
      skip();
   }
   /* FONTFAMILY TAGS */
   else if (strcmp("fnil",code)==0)
   {
      current_state.fontfamily=NIL;
   }
   else if (strcmp("froman",code)==0)
   {
      current_state.fontfamily=ROMAN;
   }
   else if (strcmp("fswiss",code)==0)
   {
      current_state.fontfamily=SWISS;
   }
   else if (strcmp("fmodern",code)==0)
   {
      current_state.fontfamily=MODERN;
   }
   else if (strcmp("fscript",code)==0)
   {
      current_state.fontfamily=SCRIPT;
   }
   else if (strcmp("fdecor",code)==0)
   {
      current_state.fontfamily=DECOR;
   }
   else if (strcmp("ftech",code)==0)
   {
      current_state.fontfamily=TECH;
   }
   else if (strcmp("fbidi",code)==0)
   {
      current_state.fontfamily=BIDI;
   }

   /* TEXT DECORATION */
   /* BOLD */
   else if (strcmp("b",code)==0)
   {
      current_state.decoration|=BOLD;
   }
   else if (strcmp("b0",code)==0)
   {
      current_state.decoration&=~BOLD;
   }
   /* ITALIC */
   else if (strcmp("i",code)==0)
   {
      current_state.decoration|=ITALIC;
   }
   else if (strcmp("i0",code)==0)
   {
      current_state.decoration&=~ITALIC;
   }
   /* UNDERLINE */
   else if (strcmp("ul0",code)==0 || strcmp("ulnone",code)==0)
   {
      current_state.decoration&=~UNDERLINE;
   }
   else if (strncmp("ul",code,2)==0)
   {
      current_state.decoration |= UNDERLINE;
   }
   /* STRIKE */
   else if (strcmp("strike",code)==0 || strcmp("strikedl",code)==0)
   {
      current_state.decoration |= STRIKE;
   }

   /* PARAGRAPH CODES */
   /* NEW PARAGRAPH */
   else if (strcmp("par",code)==0)
   {
      current_state.paragraph &= ~PWRITE;
   }
   else if(strcmp("pard",code)==0)
   {
      paragraph_default();
   }
   /* LEFT JUSTIFY */
   else if (strcmp("ql",code)==0)
   {        
      current_state.paragraph &= ALIGNCLEAR;
      current_state.paragraph |= LEFT;
   }
   /* RIGHT JUSTIFY */
   else if (strcmp("qr",code)==0)
   {     
      current_state.paragraph &= ALIGNCLEAR;
      current_state.paragraph |= RIGHT;
   }
   /* CENTER JUSTIFY */
   else if (strcmp("qc",code)==0)
   {         
      current_state.paragraph &= ALIGNCLEAR;
      current_state.paragraph |= CENTER;
   }
   /* JUSTIFY */
   else if (strcmp("qj",code)==0)
   {         
      current_state.paragraph &= ALIGNCLEAR;
      current_state.paragraph |= JUSTIFY;
   }
   /* DEFAULT STATE */
   else if (strcmp("plain",code)==0)
   {
      plain_text();
   }

   /* SPECIAL CHARACTERS */
   else if (strcmp("tab",code)==0 && options.expandtabs)
   {
      putstring("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
   }
   else if (strcmp("lquote",code)==0)
   {
      putstring("&lsquo;");
   }
   else if (strcmp("rquote",code)==0)
   {
      putstring("&rsquo;");
   }
   else if (strcmp("ldblquote",code)==0)
   {
      putstring("&ldquo;");
   }
   else if (strcmp("rdblquote",code)==0)
   {
      putstring("&rdquo;");
   }
   else if (code[0]=='\'')
   {      /* 'hh */
      putstring("&#x");
      xpos+=fprintf(out,"%s;",&code[1]);
   }
   else if (strcmp("_",code)==0)
   {
      putstring("-");
   }
   else if (strcmp("~",code)==0)
   {
      putstring("&nbsp;");
   }
   else if (strcmp("\\",code)==0)
   {
      putstring("\\");
   }
   else if (strcmp("{",code)==0)
   {
      putstring("{");
   }
   else if (strcmp("}",code)==0)
   {
      putstring("}");
   }
   
   /* Partial tags */
   /* default tag */
   else if (strncmp("def",code,3)==0)
   {
      set_defaults(&code[3]);
   }
   /* Set charset */
   else if (strncmp("fcharset",code,8)==0)
   {
      current_state.character_set=atoi(&code[8]);
   }
   /* FONTSIZE */
   else if (strncmp("fs",code,2)==0)
   {
      if (isdigit(code[2])) 
         current_state.fontsize=atoi(&code[2]);
   }
   /* Change font */
   else if (strncmp("f",code,1)==0)
   {
      if (isdigit(code[1]))
         current_state.font=atoi(&code[1]);
   }

   /* COLORS */
   /* Set foreground */
   else if (strncmp("cf",code,2)==0)
   {
      if (isdigit(code[2]))
      {
         current_state.fg=atoi(&code[2]);
      }
   }
   /* Set Background */
   else if (strncmp("cb",code,2)==0)
   {
      if (isdigit(code[2])) 
         current_state.bg=atoi(&code[2]);
   }
   return code;
}

static void plain_text()
{
   short para;

   para=current_state.paragraph;
   current_state=default_state;
   current_state.paragraph=para;

}

static void paragraph_default()
{
   current_state.paragraph=default_state.paragraph;
}

static void initialize()
{
   int i;
   
   depth=0;
   ss=stack_create(sizeof(state));
   hts=stack_create(TAGSIZE+1);
   current_state=default_state;
   strcpy(font_table[0],"Arial");
   memset(color_table,AUTO,sizeof(int)*MAXCOLORS);
   for (i = 0; i < MAXSTYLES; i++)
   {
      stylesheet[i]=default_state;
   }
}

/* Skips to the end of a section leaving the } in the stream  */
static void skip()
{
   char c;
   int tmp=depth;
   while(1)
   {
      c=getc(in);
      if (c=='{') depth++;
      if (c=='}')
      {
         if (depth==tmp)
            break;
         else
            depth--;
      }
   }
   fseek(in,-1,SEEK_CUR);
}

static void read_color_table()
{
   char c;
   char *code;
   int color;
   int i=0;

   printl(DEBUG,"read_color_table","Reading color table\n");
   c=getc(in);

   while(c != '}')
   {
      if (c=='\\')
      {
         code=translate();
         if (strncmp(code,"red",3)==0)
         {
            color = atoi(&code[3]);
            color*=0x10000;
            color_table[i] |= color;
         }
         else if (strncmp(code,"green",5)==0)
         {
            color = atoi(&code[5]);
            color*=0x100;
            color_table[i] |= color;
         }
         else if (strncmp(code,"blue",4)==0)
         {
            color = atoi(&code[4]);
            color_table[i] |= color;
         }
      }
      else if(c==';')
      {
         printl(DEBUG,"read_color_table","0x%06X written to table\n",color_table[i]);
         i++;
      }
      c=getc(in);
   }
   fseek(in,-1,SEEK_CUR);
}

/* TODO: stylesheets are as of yet unimplementd */
static void read_style_sheet()
{
   skip();
}

/* This function reads in all fonts to be used  */
static void read_font_table()
{
   char c;
   char* code;
   char font_name[128];
   int tmp=depth;
   int write_font=0;
   int loc=0;

   printl(DEBUG,"read_font_table","Reading Font Table\n");
   while (1)
   {
      c=getc(in);
      switch (c)
      {
      case 0x0a:
      case 0x0d: /*ignore  */
         break;
      case '{':
         push_state();
         break;
      case '}':
         if (tmp==depth)
         {
            fseek(in,-1,SEEK_CUR);
            return;
         }
         printl(DEBUG,"read_font_table","Writing \"%s\" into table\n",font_name);
         strcpy(font_table[current_state.font],font_name);
         font_family[current_state.font]=current_state.fontfamily;
         write_font=0;
         loc=0;
         pop_state();
         break;
      case '\\':
         code = translate();
         break;

      case ';':
         write_font=0;
         break;
      default:
         if (c!=' ')
            write_font=1;
         if (write_font && loc < MAXFONTNAME)
         {
            font_name[loc]=c;
            loc++;
            font_name[loc]=0;
         }
      }
   }
}

static void read_header()
{
   int tmp=depth;
   char c;
   while(1)
   {
      c = getc(in);
      if (c=='\\')
         translate();
      else if (c=='{')
      {
         push_state();
      }
      else if (c=='}')
      {
         if (depth==tmp)
            break;
         pop_state();
      }
      else if (c==0x0a || c==0x0d || c==' ');
      else
         break;

   }
   fseek(in,-1,SEEK_CUR);
}

void set_indent(const char* p)
{
   char tmp[13];
   char* ptr=tmp;
   char* fnd;
   
   strncpy(tmp,p,12);
   if (tmp[0] == '\'' || tmp[0] == '\"')
   {
      if (tmp[strlen(tmp)-1] != '\'')
      {
         printl(WARNING,"set_indent","Unmatched %c using default: \'   \'\n",tmp[0]);
         return;
      }
      tmp[strlen(tmp)-1] = 0;
      ptr++;
   }
   while((fnd=strstr(ptr,"\\t")))
   {
      fnd[0] ='\t';
      strcpy(&fnd[1],&fnd[2]);
   }
   strncpy(SPACER, ptr, 10-strlen(SPACER));
   printl(INFO,"set_indent","Setting indent string to \"%s\"\n",SPACER);
}

void putstring(const char* str)
{
   if(memcmp(&previous_state, &current_state,sizeof(state)) != 0)
      change_state();
   xpos += fprintf(out,str);
   last_char=CHARACTER;
}

void rtf2xhtml(const char* infile, const char* outfile)
{
   char c;
   char* code;
   char str[64];
   char tag[TAGSIZE];

   in=fopen(infile,"r");
   if (in==NULL)
   {
      printl(ERROR,"rtf2xhtml","Cannot open %s\n",infile);
      return;
   }
   out=fopen(outfile,"w");
   if (out==NULL)
   {
      printl(ERROR,"rtf2xhtml","Cannot open %s for writing\n",outfile);
      fclose(in);
      return;
   }
   if (!in || !out)
   {
      cleanup();
      return;
   }
   initialize();
   if (getc(in) != '{')
   {
      c=getc(in);
      while(c==0x0a||c==0x0d)
         c=getc(in);
      code=translate();
      if (strncmp("rtf",code,3) != 0)
      {
         printl(ERROR,"rtf2xhtml","%s is not a valid rtf document\n", infile);
         cleanup();
         return;
      }
      else
         printl(INFO,"rtf2xhtml","Document %s is rtf version %s\n", infile, &code[3]);
   }
   printl(INFO,"rtf2xhtml","%s is now being converted to %s\n",infile,outfile);
   fprintf(out,"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n"
            "   \"http://www.w3.org/TR/xhtml1/DTD/xhtml11.dtd\">\n");
   last_char=NEWLINE;
   push_tag("html","",0);
   previous_state=current_state;
   push_state();
   push_tag("head","",0);
   push_tag("meta"," name=\"generator\" content=\"rtf2xhtml\"",1);
   read_header();
   pop_tag(tag);
   push_tag("body","",0);
   while ((c = getc(in)) != EOF)
   {
      switch (c)
      {
         case '{':
            push_state();
            break;
         case '}':
            pop_state();
            break;
         case '&':
            putstring("&amp;");
         case '\\':
            translate();
            break;
         case 0x0d:
         case 0x0a:  
            break;
         case 0x09:
         case ' ':
            if(last_char!=INDENT && last_char!=SPACE)
            {
               putc(' ',out);
               last_char=SPACE;
               xpos++;
               if (xpos > 80)
               {
                  putc('\n',out);
                  xpos=fprintf(out,"%s",indent);
                  last_char=INDENT;
               }
            }
            break;
         default:
            str[0] = c;
            str[1] = 0;
            putstring(str);
      }
   }
   pop_tag(tag);

   printl(INFO,"rtf2xhtml","%s to %s conversion complete!\n",infile,outfile);
   cleanup();
}

static void cleanup()
{
   char tag[TAGSIZE+1];
   while(pop_state());
   while(pop_tag(tag));
   stack_destroy(ss);
   fclose(in);
   fclose(out);
}
