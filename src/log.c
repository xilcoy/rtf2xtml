/***************************************************************************
                          log.c  -  This file is for logging events
                             -------------------
    begin                : Mon Mar 8 2004
    copyright            : (C) 2004 by Coy D. Barnes
    email                : coy.barnes@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <log.h>

int log_level;
int log_file;

void printl(short type, const char* function, const char* fmt, ...)
{
   char format[255];
   char str[512];
   char* p;
   time_t current_time;
   
   va_list arglist;
   FILE* fp1 = NULL;
   FILE* fp2 = NULL;

   memset(format,0,255);
   memset(str,0,255);
   
   current_time=time(NULL);
   p = ctime(&current_time);
   p[strlen(p)-1]=0;
   
   if (log_level < ERROR || log_level > ALL)
   {
      log_level = INFO;
   }

   if (type == ERROR)
   {
      strcpy( format, "ERROR:   " );
      fp1 = stderr;
      if (log_file)
      {
         fp2 = fopen("log.txt","a");
      }
   }
   else if (type == WARNING)
   {
      if (log_level >= WARNING)
      {
         strcpy( format, "WARNING: " );
         fp1 = stdout;
         if (log_file)
         {
            fp2 = fopen("log.txt","a");
         }
      }
   }
   else if (type == INFO)
   {
      if (log_level >= INFO)
      {
         strcpy( format, "INFO:    " );
         if (log_level == ALL)
         {
            fp1 = stdout;
         }
         if (log_file)
         {
            fp2 = fopen("log.txt","a");
         }
      }
   }
   else
   {
      if (log_level >= DEBUG)
      {
         strcpy( format, "DEBUG:   " );
         fp1 = stdout;
         if (log_level==ALL && log_file)
         {
            fp2=fopen("log.txt","a");
         }
      }
   }
   strncat(format,function,79);
   strcat(format,": ");
   strncat(format,fmt,79);

   va_start( arglist, fmt );
      vsprintf(str,format,arglist);
      if (fp1 != NULL)
      {
         fprintf(fp1, str);
      }
      if (fp2 != NULL)
      {
         fprintf(fp2,"%s %s", p, str);
         fclose(fp2);
      }
    va_end( arglist );
}

