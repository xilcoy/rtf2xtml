/***************************************************************************
              stack.c  -  This is a generic stack implementation used for
                          both the rtf state stack, and the html tag stack
                             -------------------
    begin                : Mon Mar 8 2004
    copyright            : (C) 2004 by Coy D. Barnes
    email                : coy.barnes@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <malloc.h>
#include <errno.h>
#include <string.h>
#include <stack.h>

#ifdef MSVC
#pragma warning(disable:4700)
#endif

#ifndef NULL
#define NULL 0
#endif

#define sn_alloc() (stack_node*)malloc(sizeof(struct stack_node))
#define stack_alloc() (stack*)malloc(sizeof(struct stack))

static stack_node* create_node(int size)
{
	stack_node* ret;
	ret = sn_alloc();
	if (ret == NULL)
   {
		return NULL;
   }
	ret->next=NULL;
	ret->item = malloc(size);
	return ret;
}

static void delete_node(stack_node* p)
{
	if (p == NULL)
   {
		return;
   }
	free(p->item);
	free(p);
}

stack *stack_create(int size)
{
	stack* ret;
	ret = stack_alloc();
	if (ret == NULL)
   {
		return NULL;
   }
	ret->top = NULL;
	ret->size = size;
	return ret;
}

static void destroy_r(stack_node* p,int size)
{
	if (p==NULL)
   {
		return;
	}
	if (p->next != NULL)
   {
		destroy_r(p->next,size);
   }
	delete_node(p);
}

void stack_destroy(stack* p)
{
	if (p==NULL)
	{
		return;
	}
	destroy_r(p->top,p->size);
}

int stack_push(stack* p, const void *item)
{
	stack_node *tmp;
	if (p==NULL)
   {
		return 0;
	}
	if (p->top==NULL)
   {
		tmp = create_node(p->size);
		if(tmp==NULL) 
      {
			return 0;
      }
		p->top=tmp;
		memcpy(p->top->item,item,p->size);
		p->top->next = NULL;
		return 1;
	}
	tmp = create_node(p->size);
	tmp->next=p->top;
	memcpy(tmp->item,item,p->size);
	p->top=tmp;
	return 1;
}

int stack_pop(stack* p,void *item)
{
	stack_node* tmp;
	if (p == NULL)
   {
		return 0;
   }
	if (p->top==NULL)
   {
		return 0;
   }
	memcpy(item, p->top->item,p->size);
	tmp = p->top;
	p->top = p->top->next;
	delete_node(tmp);
	return 1;
}

int stack_peek(stack* p,void *item)
{
	if (p == NULL)
   {
		return 0;
   }
	if (p->top==NULL)
   {
		return 0;
   }
	memcpy(item, p->top->item,p->size);
	return 1;
}

int stack_size(stack* p)
{
	stack_node *tmp;
	int ret = 0;
	for(tmp=p->top;tmp;tmp=tmp->next)
   {
		ret++;
   }
	return ret;
}
