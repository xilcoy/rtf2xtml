/***************************************************************************
                          log.h  -  This file is for logging events
                             -------------------
    begin                : Mon Mar 8 2004
    copyright            : (C) 2004 by Coy D. Barnes
    email                : dec0y@hotpop.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _rtf2html_log_h_
#define _rtf2html_log_h_

#define ERROR	0
#define WARNING	1
#define INFO	2
#define DEBUG	3
#define ALL		4

extern int log_level;
extern int log_file;

void printl(short type, const char* function, const char* fmt, ...);

#endif

