/***************************************************************************
              rtfparser.h  -  This file contains most of the necessary code
                              to convert a document from rtf to html
                             -------------------
    begin                : Mon Mar 8 CST 2004
    copyright            : (C) 2004 by Coy D. Barnes
    email                : dec0y@hotpop.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _rtf_parser_h_
#define _rtf_parser_h_

#include <stdio.h>
#include "stack.h"
#define TAGSIZE 50
#define DEPTH 50
#define WIDTH 80
#define MAXFONTS 50
#define MAXFONTNAME 100
#define MAXCOLORS 255
#define MAXSTYLES 50

extern char SPACER[11];

typedef struct
{
	short font;
	short fontsize;
	char fontfamily;
	short decoration;
	short style;
	short character_set;
	short language;
	short bg;
	short fg;
	short paragraph;
} state;

void set_indent(const char*);
void rtf2xhtml(const char*, const char*);

#endif

