/***************************************************************************
              stack.h  -  This is a generic stack implementation used for
                          both the rtf state stack, and the html tag stack
                             -------------------
    begin                : Mon Mar 8 2004
    copyright            : (C) 2004 by Coy D. Barnes
    email                : dec0y@hotpop.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _Coys_stack_
#define _Coys_stack_

typedef struct stack_node
{
	void *item;
	struct stack_node* next;
} stack_node;

typedef struct stack
{
	stack_node* top;
	int size;
} stack;

stack *stack_create(int);
void stack_destroy(stack*);
int stack_push(stack*,const void*);
int stack_pop(stack*,void*);
int stack_peek(stack*,void*);
int stack_size(stack*);

#endif

