struct
{
   unsigned int font       : 1;
   unsigned int fontsize   : 1;
   unsigned int headings   : 1;
   unsigned int color      : 1;
   unsigned int align      : 1;
   unsigned int justify    : 1;
   unsigned int stylesheet : 1;
   unsigned int expandtabs : 1;
} options;

void options_init(void);
void options_read(const char*);
void options_write(const char*);
